package com.company.optfirm.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "dicProductCategory")
@Entity(name = "optfirm_DicProductCategory")
@NamePattern("%s|categoryName")
public class DicProductCategory extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 2081722911879538528L;
    @Column(name = "categoryName", nullable = false, length = 100)
    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
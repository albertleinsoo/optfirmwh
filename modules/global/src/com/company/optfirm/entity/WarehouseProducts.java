package com.company.optfirm.entity;

import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.*;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "warehouseProducts")
@Entity(name = "optfirm_WarehouseProducts")
public class WarehouseProducts extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 6176249115156388170L;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product")
    private com.company.optfirm.entity.DicProduct product;

    @Column(name = "productQuantity", nullable = false)
    private Integer productQuantity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "warehouse")
    private com.company.optfirm.entity.DicWarehouse warehouse;

    public com.company.optfirm.entity.DicWarehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(com.company.optfirm.entity.DicWarehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public com.company.optfirm.entity.DicProduct getProduct() {
        return product;
    }

    public void setProduct(com.company.optfirm.entity.DicProduct product) {
        this.product = product;
    }
}
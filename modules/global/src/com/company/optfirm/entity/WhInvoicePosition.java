package com.company.optfirm.entity;

import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DdlGeneration;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "whInvoicePosition")
@Entity(name = "optfirm_WhInvoicePosition")
public class WhInvoicePosition extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = -6127235823679834999L;

    @NotNull
    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup", "open", "clear"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "invoice")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private com.company.optfirm.entity.WhInvoice invoice;

    @Column(name = "positionNum", nullable = false)
    private Integer positionNum;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product")
    private com.company.optfirm.entity.DicProduct product;

    @Column(name = "productQuantity")
    private Integer productQuantity;

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public com.company.optfirm.entity.DicProduct getProduct() {
        return product;
    }

    public void setProduct(com.company.optfirm.entity.DicProduct product) {
        this.product = product;
    }

    public Integer getPositionNum() {
        return positionNum;
    }

    public void setPositionNum(Integer positionNum) {
        this.positionNum = positionNum;
    }

    public com.company.optfirm.entity.WhInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(com.company.optfirm.entity.WhInvoice invoice) {
        this.invoice = invoice;
    }
}
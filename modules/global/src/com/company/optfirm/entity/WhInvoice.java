package com.company.optfirm.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.security.entity.User;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "whInvoice")
@Entity(name = "optfirm_WhInvoice")
@NamePattern("%s - %s|docNumber,docDate")
public class WhInvoice extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = -2341112776846938771L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "docDate", nullable = false)
    private Date docDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "docDateProcessed")
    private Date docDateProcessed;

    @Column(name = "docNumber", nullable = false, length = 100)
    private String docNumber;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "docType")
    private com.company.optfirm.entity.DicDocumentType docType;

    @Column(name = "userCreated", nullable = false, length = 100)
    private String userCreated;

    @Column(name = "userProcessed", length = 100)
    private String userProcessed;

    @Composition
    @OnDeleteInverse(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "invoice")
    private List<WhInvoicePosition> whInvoicePositionList;

    @PostConstruct
    protected void initWhInvoiceUserCreated(){

        UserSessionSource ussr = AppBeans.get(UserSessionSource.NAME);

        User currentUser = ussr.getUserSession().getUser();

        if(currentUser == null)
        {
            throw new IllegalArgumentException("User is not logged In!");
        }

        userCreated = currentUser.getLogin() + "( " + currentUser.getName() + " )";
    }

    @PostConstruct
    protected void initWhInvoiceDateTimeCreated(){

        TimeSource timeSource = AppBeans.get(TimeSource.class);

        docDate = timeSource.currentTimestamp();
    }

    public List<WhInvoicePosition> getWhInvoicePositionList()
    {
        return whInvoicePositionList;
    }

    public void setWhInvoicePositionList(List<WhInvoicePosition> whInvoicePositionList)
    {
        this.whInvoicePositionList = whInvoicePositionList;
    }

    public String getUserProcessed() {
        return userProcessed;
    }

    public void setUserProcessed(String userProcessed) {
        this.userProcessed = userProcessed;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public com.company.optfirm.entity.DicDocumentType getDocType() {
        return docType;
    }

    public void setDocType(com.company.optfirm.entity.DicDocumentType docType) {
        this.docType = docType;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public Date getDocDateProcessed() {
        return docDateProcessed;
    }

    public void setDocDateProcessed(Date docDateProcessed) {
        this.docDateProcessed = docDateProcessed;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }
}
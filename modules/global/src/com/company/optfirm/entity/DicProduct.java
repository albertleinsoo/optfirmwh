package com.company.optfirm.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.*;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "dicProduct")
@Entity(name = "optfirm_DicProduct")
@NamePattern("%s|productName")
public class DicProduct extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = -6812544035961751340L;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "productCategory")
    private DicProductCategory productCategory;

    @Column(name = "productName", nullable = false, length = 100)
    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public DicProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(DicProductCategory productCategory) {
        this.productCategory = productCategory;
    }
}
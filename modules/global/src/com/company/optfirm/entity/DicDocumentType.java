package com.company.optfirm.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "dicDocumentType")
@Entity(name = "optfirm_DicDocumentType")
@NamePattern("%s|docTypeName")
public class DicDocumentType extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 4173046908347992690L;
    @Column(name = "docTypeName", nullable = false, length = 100)
    private String docTypeName;

    public String getDocTypeName() {
        return docTypeName;
    }

    public void setDocTypeName(String docTypeName) {
        this.docTypeName = docTypeName;
    }
}
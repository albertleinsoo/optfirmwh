package com.company.optfirm.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.*;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "dicWarehouse")
@Entity(name = "optfirm_DicWarehouse")
@NamePattern("%s|whName")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "whNumber"))
})
public class DicWarehouse extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 3584169701954489330L;
    @Column(name = "whName", nullable = false, length = 100)
    private String whName;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "whProductCategory")
    private DicProductCategory whProductCategory;

    public DicProductCategory getWhProductCategory() {
        return whProductCategory;
    }

    public void setWhProductCategory(DicProductCategory whProductCategory) {
        this.whProductCategory = whProductCategory;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }
}
package com.company.optfirm.web.screens.whinvoice;

import com.company.optfirm.entity.*;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.RemoveOperation;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.User;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@UiController("optfirm_WhInvoice.browse")
@UiDescriptor("wh-invoice-browse.xml")
@LookupComponent("whInvoiceTable")
@LoadDataBeforeShow
public class WhInvoiceBrowse extends StandardLookup<WhInvoice> {

    @Inject
    DataManager dataManager;

    @Inject
    Dialogs whDialogs;

    @Inject
    private GroupTable<WhInvoice> whInvoiceTable;

    @Inject
    private CollectionContainer<WhInvoice> whInvoiceDc;

    @Named("whInvoicePositionTable.create")
    private CreateAction<WhInvoicePosition> whInvoicePositionTableCreate;

    @Subscribe
    public void onInit(InitEvent event) {whInvoicePositionTableCreate.refreshState();}

    @Install(to = "whInvoicePositionTable.create" , subject = "initializer")
    private void whInvoicePositionTableInitializer(WhInvoicePosition whInvoicePosition) {
        WhInvoice whInvoice = whInvoiceTable.getSingleSelected();
        if (whInvoice == null){
            throw new IllegalStateException("Не выбрана накладная");
        }
        whInvoicePosition.setInvoice(whInvoice);
    }

    @Install(to = "whInvoicePositionTable.create",subject = "enabledRule")
    private boolean whInvoicePositionTableCreateEnableRule() {
        return whInvoiceTable.getSingleSelected() != null;
    }

    @Subscribe("whInvoiceTable.processPosition")
    protected void onWhInvoiceTableProcessActionPerformed(Action.ActionPerformedEvent event){

        WhInvoice selectedWhInvoice = whInvoiceTable.getSingleSelected();
        //whInvoicePositionTableCreate.refreshState();

        if (selectedWhInvoice.getDocDateProcessed() != null){
            throw new IllegalArgumentException("Документ уже проведён");
        }

        if (selectedWhInvoice.getWhInvoicePositionList().size() == 0){
            throw new IllegalArgumentException("В накладной отсутствуют товары.");
        }

        if(selectedWhInvoice.getDocType().getDocTypeName().equals("Приход")){
            commitWarehouseChangesOnReceipt(selectedWhInvoice);
        } else if (selectedWhInvoice.getDocType().getDocTypeName().equals("Расход")){
            commitWarehouseChangesOnRelease(selectedWhInvoice);
        } else {
            throw new IllegalArgumentException("Неизвестный тип документа");
        }

    }

    private void commitWarehouseChangesOnReceipt(WhInvoice selectedWhInvoice){
        /*обходим список whInvoicePosition*/
        for (WhInvoicePosition curWhInvoicePosition : selectedWhInvoice.getWhInvoicePositionList()){
            /*Загружаем текущую whInvoicePosition*/
            /*todo посмотреть, ожно ли догрузить в dataManager известный класс*/
            curWhInvoicePosition = dataManager.load(WhInvoicePosition.class)
                    .id(curWhInvoicePosition.getId())
                    .view("whInvoicePosition-view")
                    .one();

            /*id продукта текущей whInvoicePosition*/
            DicProduct whInvoicePositionProduct = curWhInvoicePosition.getProduct();

            /*Проверяем, есть ли продукт на складе*/
            LoadContext<WarehouseProducts> whProductContext = LoadContext.create(WarehouseProducts.class).setQuery(
                            LoadContext.createQuery("select wp from optfirm_WarehouseProducts wp where wp.product = :productId")
                                    .setParameter("productId", whInvoicePositionProduct))
                    .setView("warehouseProducts-view");
            long whPositionsCount = dataManager.getCount(whProductContext);

            if (whPositionsCount > 0) {
                /**/
                WarehouseProducts warehouseProductsToUpdate = dataManager.load(WarehouseProducts.class)
                        .query("select wp from optfirm_WarehouseProducts wp where wp.product = :productId")
                        .parameter("productId", whInvoicePositionProduct)
                        .view("warehouseProducts-view")
                        .one();

                warehouseProductsToUpdate.setProductQuantity(warehouseProductsToUpdate.getProductQuantity()
                        + curWhInvoicePosition.getProductQuantity());

                dataManager.commit(warehouseProductsToUpdate);
            } else {
                WarehouseProducts whProductInsert = dataManager.create(WarehouseProducts.class);

                whProductInsert.setProductQuantity(curWhInvoicePosition.getProductQuantity());

                whProductInsert.setProduct(whInvoicePositionProduct);

                DicProduct dicProductToInsert = dataManager.load(DicProduct.class)
                        .query("select e from optfirm_DicProduct e where e.id = :product")
                        .parameter("product", whInvoicePositionProduct.getId().intValue())
                        .view("dicProduct-view")
                        .one();

                DicProductCategory productCategoryToInsert = dataManager.load(DicProductCategory.class)
                        .query("select e from optfirm_DicProductCategory e where e.id = :pCategory")
                        .parameter("pCategory", dicProductToInsert.getProductCategory().getId().intValue())
                        .view("_local")
                        .one();

                /*Проверяем есть ли склады с такой категорией*/
                LoadContext<DicWarehouse> warehouseContext = LoadContext.create(DicWarehouse.class).setQuery(
                                LoadContext.createQuery("select e from optfirm_DicWarehouse e where e.whProductCategory = :productCategory")
                                        .setParameter("productCategory" , productCategoryToInsert))
                        .setView("dicWarehouse-view");
                long whCount = dataManager.getCount(warehouseContext);

                if(whCount == 0){
                    /*todo вместо exception вывести форму создания склада*/
                    throw new IllegalArgumentException("Нет складов с категорией товара: " + whProductInsert.getProduct().getProductCategory());
                } else {
                    DicWarehouse warehouseToInsert = dataManager.load(warehouseContext);
                    whProductInsert.setWarehouse(warehouseToInsert);
                    dataManager.commit(whProductInsert);
                }
            }
        }
        //-----

        //Сохранить  Дату проведения и Пользователя
        commitDateUserChanges(selectedWhInvoice);
    }

    private void commitWarehouseChangesOnRelease(WhInvoice selectedWhInvoice) {

        List<WhInvoicePosition> whInvoicePositionLoadList =
                dataManager.load(WhInvoicePosition.class)
                        .query("select e from optfirm_WhInvoicePosition e where e.invoice = :selectedWhInvoice")
                        .parameter("selectedWhInvoice",selectedWhInvoice)
                        .view("whInvoicePosition-view")
                        .list();

        String positionCountErrorString = "";

        for (WhInvoicePosition curWhInvoicePosition : whInvoicePositionLoadList){

            LoadContext<WarehouseProducts> whProductContext = LoadContext.create(WarehouseProducts.class).setQuery(
                            LoadContext.createQuery("select wp from optfirm_WarehouseProducts wp where wp.product = :productId")
                                    .setParameter("productId", curWhInvoicePosition.getProduct()))
                    .setView("warehouseProducts-view");
            long whPositionsCount = dataManager.getCount(whProductContext);
            if (whPositionsCount == 0){
                positionCountErrorString = positionCountErrorString.concat("Товар \"" + curWhInvoicePosition.getProduct().getProductName()
                        + "\" отсутствует на складах\n");
                continue;
            }

            WarehouseProducts whProductsToRelease = dataManager.load(whProductContext);
            if (whProductsToRelease.getProductQuantity() < curWhInvoicePosition.getProductQuantity()){
                positionCountErrorString = positionCountErrorString.concat("Товара \"" + curWhInvoicePosition.getProduct().getProductName()
                        + "\" недостаточно для выдачи\n");
            }
        }

        if (positionCountErrorString.equals("")){
            for (WhInvoicePosition curWhInvoicePosition : whInvoicePositionLoadList){

                LoadContext<WarehouseProducts> whProductContext = LoadContext.create(WarehouseProducts.class).setQuery(
                                LoadContext.createQuery("select wp from optfirm_WarehouseProducts wp where wp.product = :productId")
                                        .setParameter("productId", curWhInvoicePosition.getProduct()))
                        .setView("warehouseProducts-view");

                WarehouseProducts whProductsToRelease = dataManager.load(whProductContext);

                whProductsToRelease.setProductQuantity(whProductsToRelease.getProductQuantity()
                        - curWhInvoicePosition.getProductQuantity());

                dataManager.commit(whProductsToRelease);
            }

            //Сохранить  Дату проведения и Пользователя
            commitDateUserChanges(selectedWhInvoice);

        } else {
            throw new IllegalArgumentException(positionCountErrorString);
        }
    }

    protected void commitDateUserChanges (WhInvoice selectedWhInvoice){

        UserSessionSource ussr = AppBeans.get(UserSessionSource.NAME);

        User currentUser = ussr.getUserSession().getUser();

        selectedWhInvoice.setUserProcessed(currentUser.getLogin() + "( " + currentUser.getName() + " )");

        TimeSource timeSource = AppBeans.get(TimeSource.class);

        selectedWhInvoice.setDocDateProcessed(timeSource.currentTimestamp());

        //Сохранить  Дату проведения и Пользователя
        dataManager.commit(selectedWhInvoice);

    }


    @Install(to = "whInvoicePositionTable.create", subject = "afterCommitHandler")
    private void whInvoicePositionTableCreateAfterCommitHandler(WhInvoicePosition whInvoicePosition) {
        whInvoicePosition.getInvoice();
        WhInvoice curWhInvoice = dataManager.reload(whInvoicePosition.getInvoice(),
                "whInvoice-view");
        whInvoiceDc.replaceItem(curWhInvoice);
    }

    @Install(to = "whInvoicePositionTable.edit", subject = "afterCommitHandler")
    private void whInvoicePositionTableEditAfterCommitHandler(WhInvoicePosition whInvoicePosition) {
        whInvoicePosition.getInvoice();
        WhInvoice curWhInvoice = dataManager.reload(whInvoicePosition.getInvoice(),
                "whInvoice-view");
        whInvoiceDc.replaceItem(curWhInvoice);
    }

    @Install(to = "whInvoicePositionTable.remove", subject = "afterActionPerformedHandler")
    private void whInvoicePositionTableRemoveAfterActionPerformedHandler(RemoveOperation.AfterActionPerformedEvent<WhInvoicePosition> afterActionPerformedEvent) {
        List<WhInvoicePosition> whPosList = afterActionPerformedEvent.getItems();

        WhInvoice curWhInvoice = dataManager.reload(whPosList.get(0).getInvoice(),
                "whInvoice-view");
        whInvoiceDc.replaceItem(curWhInvoice);
    }

    @Install(to = "whInvoicePositionTable.create", subject = "enabledRule")
    private boolean whInvoicePositionTableCreateEnabledRule() {
        return WhInvoicePositionTableCheckButtonsEnabled();
    }

    @Install(to = "whInvoicePositionTable.edit", subject = "enabledRule")
    private boolean whInvoicePositionTableEditEnabledRule() {
        return WhInvoicePositionTableCheckButtonsEnabled();
    }

    @Install(to = "whInvoicePositionTable.remove", subject = "enabledRule")
    private boolean whInvoicePositionTableRemoveEnabledRule() {
        return WhInvoicePositionTableCheckButtonsEnabled();
    }

    private boolean WhInvoicePositionTableCheckButtonsEnabled (){
        WhInvoice selectedWhInvoice = whInvoiceTable.getSingleSelected();
        if (selectedWhInvoice != null) {
            if (selectedWhInvoice.getDocDateProcessed() == null) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

}
package com.company.optfirm.web.screens.whinvoice;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.WhInvoice;

@UiController("optfirm_WhInvoice.edit")
@UiDescriptor("wh-invoice-edit.xml")
@EditedEntityContainer("whInvoiceDc")
@LoadDataBeforeShow
public class WhInvoiceEdit extends StandardEditor<WhInvoice> {
}
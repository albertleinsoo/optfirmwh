package com.company.optfirm.web.screens.dicwarehouse;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicWarehouse;

@UiController("optfirm_DicWarehouse.edit")
@UiDescriptor("dic-warehouse-edit.xml")
@EditedEntityContainer("dicWarehouseDc")
@LoadDataBeforeShow
public class DicWarehouseEdit extends StandardEditor<DicWarehouse> {
}
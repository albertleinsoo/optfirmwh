package com.company.optfirm.web.screens.dicdocumenttype;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicDocumentType;

@UiController("optfirm_DicDocumentType.browse")
@UiDescriptor("dic-document-type-browse.xml")
@LookupComponent("dicDocumentTypesTable")
@LoadDataBeforeShow
public class DicDocumentTypeBrowse extends StandardLookup<DicDocumentType> {
}


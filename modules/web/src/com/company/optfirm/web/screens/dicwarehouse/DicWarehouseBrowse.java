package com.company.optfirm.web.screens.dicwarehouse;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicWarehouse;

@UiController("optfirm_DicWarehouse.browse")
@UiDescriptor("dic-warehouse-browse.xml")
@LookupComponent("dicWarehousesTable")
@LoadDataBeforeShow
public class DicWarehouseBrowse extends StandardLookup<DicWarehouse> {
}
package com.company.optfirm.web.screens.warehouseproducts;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.WarehouseProducts;

@UiController("optfirm_WarehouseProducts.browse")
@UiDescriptor("warehouse-products-browse.xml")
@LookupComponent("warehouseProductsesTable")
@LoadDataBeforeShow
public class WarehouseProductsBrowse extends StandardLookup<WarehouseProducts> {
}
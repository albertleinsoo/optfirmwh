package com.company.optfirm.web.screens.whinvoiceposition;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.WhInvoicePosition;

@UiController("optfirm_WhInvoicePosition.browse")
@UiDescriptor("wh-invoice-position-browse.xml")
@LookupComponent("whInvoicePositionsTable")
@LoadDataBeforeShow
public class WhInvoicePositionBrowse extends StandardLookup<WhInvoicePosition> {
}
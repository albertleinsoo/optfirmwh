package com.company.optfirm.web.screens.dicproduct;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicProduct;

@UiController("optfirm_DicProduct.edit")
@UiDescriptor("dic-product-edit.xml")
@EditedEntityContainer("dicProductDc")
@LoadDataBeforeShow
public class DicProductEdit extends StandardEditor<DicProduct> {
}
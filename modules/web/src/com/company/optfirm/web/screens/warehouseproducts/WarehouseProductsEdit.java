package com.company.optfirm.web.screens.warehouseproducts;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.WarehouseProducts;

@UiController("optfirm_WarehouseProducts.edit")
@UiDescriptor("warehouse-products-edit.xml")
@EditedEntityContainer("warehouseProductsDc")
@LoadDataBeforeShow
public class WarehouseProductsEdit extends StandardEditor<WarehouseProducts> {
}
package com.company.optfirm.web.screens.whinvoiceposition;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.WhInvoicePosition;

@UiController("optfirm_WhInvoicePosition.edit")
@UiDescriptor("wh-invoice-position-edit.xml")
@EditedEntityContainer("whInvoicePositionDc")
@LoadDataBeforeShow
public class WhInvoicePositionEdit extends StandardEditor<WhInvoicePosition> {
}
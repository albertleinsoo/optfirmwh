package com.company.optfirm.web.screens.dicdocumenttype;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicDocumentType;

@UiController("optfirm_DicDocumentType.edit")
@UiDescriptor("dic-document-type-edit.xml")
@EditedEntityContainer("dicDocumentTypeDc")
@LoadDataBeforeShow
public class DicDocumentTypeEdit extends StandardEditor<DicDocumentType> {
}
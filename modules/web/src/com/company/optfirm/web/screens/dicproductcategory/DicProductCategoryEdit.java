package com.company.optfirm.web.screens.dicproductcategory;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicProductCategory;

@UiController("optfirm_DicProductCategory.edit")
@UiDescriptor("dic-product-category-edit.xml")
@EditedEntityContainer("dicProductCategoryDc")
@LoadDataBeforeShow
public class DicProductCategoryEdit extends StandardEditor<DicProductCategory> {
}
package com.company.optfirm.web.screens.dicproductcategory;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicProductCategory;

@UiController("optfirm_DicProductCategory.browse")
@UiDescriptor("dic-product-category-browse.xml")
@LookupComponent("dicProductCategoriesTable")
@LoadDataBeforeShow
public class DicProductCategoryBrowse extends StandardLookup<DicProductCategory> {
}
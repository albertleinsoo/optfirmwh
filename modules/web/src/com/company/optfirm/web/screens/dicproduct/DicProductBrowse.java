package com.company.optfirm.web.screens.dicproduct;

import com.haulmont.cuba.gui.screen.*;
import com.company.optfirm.entity.DicProduct;

@UiController("optfirm_DicProduct.browse")
@UiDescriptor("dic-product-browse.xml")
@LookupComponent("dicProductsTable")
@LoadDataBeforeShow
public class DicProductBrowse extends StandardLookup<DicProduct> {
}